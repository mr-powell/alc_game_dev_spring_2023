using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    //Reference for the door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;
    //Reference for the wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;
    //how many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;
    //Object to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject keyPrefab;
    public GameObject exitDoorPrefab;
    // list of postions to avoid instantiating new objects on top of old
    private List<Vector3> usedPositions = new List<Vector3>();
    

    public void GenerateInterior()
    {
        // Generate coins,enemies, health packs, and tacos, etc.
        //Do we spawn enemies?
        if(Random.value < Generation.instance.enemySpawnChance)
        {
            SpawnPrefab(enemyPrefab, 1, Generation.instance.maxEnemiesPerRoom +1);
        }
        //Do we spawn Coins?
        if(Random.value < Generation.instance.coinSpawnChance)
        {
            SpawnPrefab(coinPrefab, 1, Generation.instance.maxCoinsPerRoom +1);
        }
        //Do we spawn Health?
        if(Random.value < Generation.instance.healthSpawnChance)
        {
            SpawnPrefab(healthPrefab, 1, Generation.instance.maxHealthPerRoom +1);
        }
    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
    {
      int num = 1;

      if(min != 0 || max != 0)
        num = Random.Range(min, max);

      //for each of the prefabs
      for(int x = 0; x < num; ++x)
      {
        //Instantiate the prefab
        GameObject obj = Instantiate(prefab);
        //Get the nearest tile to a random position inside the room
        Vector3 pos = transform.position + new Vector3(Random.Range(-insideHeight / 2 , insideWidth / 2  +1) * Generation.instance.tileSize, Random.Range(-insideHeight /2, insideHeight /2 +1) * Generation.instance.tileSize, 0);
        //Place the prefab to the random position.
        obj.transform.position = pos;
        //Add the current position to the 'usedPositions' list;
        usedPositions.Add(pos);  

        //if the prefab we generated is enemyPrefab
        if(prefab == enemyPrefab)
          EnemyManager.instance.enemies.Add(obj.GetComponent<Enemy>()); //add it to the EnemyManger enemies list          
      }  
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
