using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health;
    public int damage;

    public GameObject deathDropPrefab;
    public SpriteRenderer sr;
    public PlayerController2D player;

    public LayerMask moveLayerMask;
    public float delay = 0.05f;
    public float attackChance = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController2D>();
       
    }

    public void Move()
    {
        if(Random.value < 0.5f)
            return;
        
        Vector3 dir = Vector3.zero;
        bool canMove = false;

        //before moving the enemy, get a random direction to move to. 
        while(!canMove)
        {
            dir = GetRandomDirection();
            //cast a ray into the direction;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);

            //if the ray hasn't detected any obstacle.
            if(hit.collider == null)
                canMove = true;
            
            //end of while loop
        }     
        //move towards the direction
        transform.position += dir * 0.16f;
    }

    Vector3 GetRandomDirection()
    {
        //Get a random number between 0 and 4
        int ran = Random.Range(0, 4); 

        if(ran == 0)
            return Vector3.up;
        else if(ran == 1)
            return Vector3.down;
        else if(ran == 2)
            return Vector3.left;
        else if(ran == 3)
            return Vector3.right;

        return Vector3.zero;

    }

    public void TakeDamage(int damageToTake)
    {
        health -= damageToTake;

        if(health <= 0)
        {
            if(deathDropPrefab != null)
                Instantiate(deathDropPrefab, transform.position, transform.rotation);

            Destroy(gameObject);
        }

        StartCoroutine(DamageFlash());

        if(Random.value > attackChance)
            player.TakeDamage(damage);
    }

    IEnumerator DamageFlash()
    {
        //get a reference to the default sprite color
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.white;
        // wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //Set the color back to it original color
        sr.color = defaultColor;
    }

}
