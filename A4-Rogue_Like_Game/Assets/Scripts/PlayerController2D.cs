using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController2D : MonoBehaviour
{

    public int curHP;
    public int maxHP;
    public int coins;

    public bool hasKey;

    public SpriteRenderer sr;

    // layer to avoid (mask)
    public LayerMask moveLayerMask;

    public float delay = 0.05f;

    public int damageAmount = 1;
    public float moveTileSize = 0.16f;

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);
        // If there is no moveLayerMask detected in front of the player
        if(hit.collider == null)
        {
            //Move forward
            transform.position += new Vector3(dir.x * moveTileSize, dir.y * moveTileSize, 0);
            // Move all the enemies
            EnemyManager.instance.OnPlayerMove();
        }
    }


    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }
    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }
    public void OnMoveLeft(InputAction.CallbackContext context)
    {
       if(context.phase == InputActionPhase.Performed)
            Move(Vector2.left); 
    }
    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }
    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.up);
    }
    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.down);
    }
    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.left);
    }
    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.right);
    }

    public void TakeDamage(int damageToTake)
    {
        curHP -= damageToTake;
        UI.instance.UpdateHealth(curHP);
        StartCoroutine(DamageFlash());

        if(curHP <= 0)
            SceneManager.LoadScene(0);
    }

    IEnumerator DamageFlash()
    {
        //get a reference to the default sprite color
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.white;
        // wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //Set the color back to it original color
        sr.color = defaultColor;
    }

    void TryAttack(Vector2 dir)
    {
        // Ignore the layer 1 to layer 6
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, 1 << 7);

        if(hit.collider != null)
        {
            hit.transform.GetComponent<Enemy>().TakeDamage(damageAmount);  
        }
    }
    
   public void AddCoins(int amount)
   {
       coins += amount;
       UI.instance.UpdateCoinText(coins);
   }

   public bool AddHealth (int amount)
   {
       if(curHP + amount <= maxHP)
       {
           curHP += amount;
           //Update the UI
           UI.instance.UpdateHealth(curHP);
           return true;
       }

       return false;
   }
   
    /*
    private float hInput;//Store horizontal input from the keyboard
    private float vInput;//Store vertical input from the keyboard
    public float speed;// Set player move speed
    public float turnSpeed;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       hInput = Input.GetAxis("Horizontal");// Get Horizontal Input from the keyboard
       vInput = Input.GetAxis("Vertical");// Get Vertical Input from the keyboard

       transform.Translate(Vector2.right * speed * hInput * Time.deltaTime);// Move the player in the world horizontally  
       transform.Translate(Vector2.up * speed * vInput * Time.deltaTime); // Move the player in the world vertically 
    }*/
}
